// C program for array implementation of stack
#include "stack.h"

Stack* createStack(unsigned capacity) {
	Stack* stack = (Stack*) malloc(sizeof(Stack));
	stack->capacity = capacity;
	stack->top = -1;
	stack->array = (int*) malloc(capacity * sizeof(int));
	return stack;
}

int length(Stack* stack) {
	return stack->top + 1;
}

int isFull(Stack* stack) {
	return length(stack) == stack->capacity; 
} 

int isEmpty(Stack* stack){   
	return length(stack) == 0; 
}

void push(Stack* stack, int item) {
	if (isFull(stack)) return;
	stack->array[++stack->top] = item;
	printf("%d pushed to stack \n", item);
}

int peek(Stack* stack) {
	if (isEmpty(stack)) return INT_MIN;
	return stack->array[stack->top];
}

int pop(Stack* stack) {
	if (isEmpty(stack)) return INT_MIN;
	return stack->array[stack->top--];
}

void destroy(Stack* stack) {
  free(stack->array);
  free(stack);
}










// C program for array implementation of stack
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
 
typedef struct {
    int top;
    unsigned capacity;
    int* array;
} Stack;
 
Stack* createStack(unsigned capacity);
int length(Stack* stack);
int isFull(Stack* stack);
int isEmpty(Stack* stack);
void push(Stack* stack, int item);
int pop(Stack* stack);
int peek(Stack* stack);
void destroy(Stack* stack);


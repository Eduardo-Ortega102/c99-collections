// C program for array implementation of stack
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "stack.h"


// Driver program to test above functions
int main() {
	Stack* stack = createStack(3);
	push(stack, 10);
	push(stack, 20);
	push(stack, 30);


	for (int i = 1; i <= 3; i++){
		printf("stack length = %d \n", length(stack));
		printf("%d popped from stack\n", pop(stack));
	}
	printf("stack length = %d \n", length(stack));
	destroy(stack);
	return 0;
}
